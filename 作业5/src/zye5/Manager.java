package zye5;

public class Manager extends Person {
	
	public Manager() {
		super();
	}
	public Manager(String id,String pwd,int role) {
		super(id,pwd,role);
	}
	
	public void menu(Person[] persons) {
		boolean flag=true;
		while(flag) {
			System.out.println("输入1录入账号，输入2查找所有账号信息,输入3查看个人信息，输入4修改密码");
			int n=KB.scanInt();
			switch(n) {
			case 1:
				this.save(persons);
				break;
			case 2:
				this.searchAll(persons);
				break;
			case 3:
				this.search();
				break;
			case 4:
				this.changepwd();
				break;
			default:
				flag=false;
				break;
			}
		}
	}
	public void save(Person[] p) {
		System.out.println("输入1录入教师，输入2录入学生");
		int n=KB.scanInt();
		if(n==1) {
			System.out.println("请输入账号和密码");
			p[KB.count(p)]=new Teacher(KB.scan(),KB.scan(),Person.teacher);
		}
		else if(n==2) {
			System.out.println("请输入账号和密码");
			p[KB.count(p)]=new Student(KB.scan(),KB.scan(),Person.student);
		}
	}
	public void searchAll(Person[] persons) {
		for(int i=0;i<persons.length;i++) {
			if(persons[i]!=null) {
				persons[i].search();
			}
		}
	}
	public void search() {
		super.search();
	}
	public void changepwd() {
		super.changepwd();
	}
	
}
