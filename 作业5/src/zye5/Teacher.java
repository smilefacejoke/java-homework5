package zye5;

public class Teacher extends Person{
	
	public Teacher() {
		super();
	}
	public Teacher(String id,String pwd,int role) {
		super(id,pwd,role);
	}
	
	public void save(Person[] persons) {
		System.out.println("请输入学生的账号");
		String name=KB.scan();
		for(int i=0;i<persons.length;i++) {
			if(persons[i]!=null&&persons[i].getID().equals(name)&&persons[i].getrole()==Person.student){
				System.out.print("请输入学生成绩");
				((Student)persons[i]).setscore(KB.scanInt());
			}
		}
	}
	public void searchAll(Person[] persons) {
		for(int i=0;i<persons.length;i++) {
			if(persons[i]!=null&&persons[i].getrole()==Person.student) {
				persons[i].search();
			}
		}
	}
	public void search() {
		super.search();
	}
	public void changepwd() {
		super.changepwd();
	}
	
	public void menu(Person[] persons) {
		boolean flag=true;
		while(flag) {
			System.out.print("输入1录入学生信息，输入2查看个人信息，输入3查看所有学生信息，输入4修改密码");
			int n=KB.scanInt();
			switch(n) {
			case 1:
				this.save(persons);
				break;
			case 2:
				this.search();
				break;
			case 3:
				this.searchAll(persons);
				break;
			case 4:
				this.changepwd();
				break;
			
			default:
				flag=false;
				break;
			
			}
		}
	}

}
