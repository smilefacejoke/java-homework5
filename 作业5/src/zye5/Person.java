package zye5;

public abstract class Person {
	String id;
	String pwd;
	int role;
	
	public static final int manager=1;
	public static final int teacher=2;
	public static final int student=3;
	
	public Person() {
	}
	public Person(String id,String pwd,int n) {
		this.id=id;
		this.pwd=pwd;
		role=n;
	}
	public String getID() {
		return id;
	}
	public void setID(String id) {
		this.id=id;
	}
	public String getPWD() {
		return pwd;
	}
	public void setPWD(String pwd) {
		this.pwd=pwd;
	}
	public int getrole() {
		return role;
	}
	public void setrole(int n) {
		role=n;
	}
	public static Person login(Person[] persons) {
		System.out.print("请输入账号和密码：");
		String name=KB.scan();
		String pwd=KB.scan();
		for(int i=0;i<persons.length;i++) {
			if(persons[i]!=null) {
				if(name.equals(persons[i].getID())&&pwd.equals(persons[i].getPWD())) {
					return persons[i];
				}
			}else break;
		}
		return null;
	}
	public void changepwd() {
		System.out.print("请输入新密码");
		setPWD(KB.scan());
	}
	public void search() {
		String roles="";
		switch(role) {
		case Person.manager:
			roles="管理员";
			break;
		case Person.teacher:
			roles="老师";
			break;
		case Person.student:
			roles="学生";
			break;
		}
		System.out.println("人员:"+roles+"  账号是"+id+"  密码是"+pwd);
	}
	public abstract void menu(Person[] persons);
	
}
