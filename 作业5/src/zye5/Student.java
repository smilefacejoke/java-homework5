package zye5;

public class Student extends Person {
	int score;
	
	public Student(){
		super();
	}
	public Student(String id,String pwd,int role){
		super(id,pwd,role);
	}
	public void setscore(int n) {
		score=n;
	}
	public int getscore() {
		return score;
	}
	public void search() {
		super.search();
		System.out.println("学生成绩是"+score);
	}
	public void changepwd() {
		super.changepwd();
	}
	@Override
	public void menu(Person[] persons) {
		// TODO 自动生成的方法存根
		boolean flag=true;
		while(flag) {
			System.out.print("输入1修改密码。输入2查看个人信息:");
			int n=KB.scanInt();
			switch(n) {
			case 1:
				this.changepwd();
				break;
			case 2:
				this.search();
				break;
				
			default:
				flag=false;
				break;
			}
		}

	}

}
