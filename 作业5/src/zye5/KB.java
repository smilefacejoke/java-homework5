package zye5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class KB {
public static String scan() {
	String str="";
	BufferedReader buf=new BufferedReader(new InputStreamReader(System.in));
	try {
		str=buf.readLine();
	}catch(IOException e) {
		e.printStackTrace();
	}
	return str;
}

public static int scanInt() {
	String str="";
	int n=0;
	BufferedReader buf=new BufferedReader(new InputStreamReader(System.in));
	try {
		str=buf.readLine();
		n=Integer.parseInt(str);
	}catch(IOException e){
	}
	return n;
}

public static int count(Person[]p) {
	int n=0;
	for(int i=0;i<p.length;i++) {
		if(p[i]!=null)n++;
	}
	return n;
}
}
